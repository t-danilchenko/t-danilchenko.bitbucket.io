import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app.jsx';
//import { Router, Route, hashHistory } from 'react-router';
import { BrowserRouter, Switch } from 'react-router-dom';

ReactDOM.render((
    <BrowserRouter>
        <App />
    </BrowserRouter>
), document.getElementById('root'));
