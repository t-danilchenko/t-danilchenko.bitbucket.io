import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {texts} from './app.jsx'

import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom';

 //import screenshot from '../images/tmp/screenshot-2018-06-16-14-45-59.jpg';

const Product = () => {
    return (
        <main>
            <div className="product-title">
                <h1 className="caption-large">{texts.title}</h1>
                <div className="caption-button">
                    {texts.price} <div className="small-caption-button">Full Refund guarantee</div>
                </div>
            </div>
            <section className="container-left product">

                <div className="product-image">
                    <img src={texts.image_url} />
                    {/*<img src={screenshot} />*/}
                    <div className="zoom-icon"></div>
                </div>
                <div className="buttons">
                    <Link to="/sizes" className="button">I like it!</Link>
                    <Link to="/feedback" className="button btn-white">I need a change</Link>
                </div>
            </section>
            <section className="container-right product-info">
                <h1 className="product-info--header minus-plus-icon active">Message from your designer<em></em></h1>
                <div className="product-info--text">
                    <div className="quote"></div>
                    <p>Here is my sketch and inspirations for your one-of-a-kind dress!</p>
                    <p>When I design a dress or any other piece, my intention is to give you grace and confidence to be yourself. As a designer, I see each person as unique. My truest satisfaction lies in seeing that sparkle of joy in clients' eyes. Thank you for the opportunity to dress you.</p>
                    <p>I designed this elegant yet understated cotton dress to be your go-to! The gorgeous royal blue color will spotlight your fair skin and golden hair and will work for both work and play. I chose light cotton fabric that holds its shape so you are always polished and comfortable in LA heat. I drew inspiration from the ocean’s stillness, classic interiors, and elegant patterns.</p>
                    <p>The silhouette is designed to highlight your legs while keeping the midsection concealed. You’ll enjoy moving freely in it! I know you are busy, so I made sure it’s easy for you to dress it down with a pair of slides or sneakers or up with sleek pumps — I promise that you’ll wear it over and for many occasions.</p>
                    <div className="user-container">
                        <div className="user-avatar">
                            <img src="http://dont-touch.me/wp-content/uploads/2018/04/learn-to-draw-realistic-faces-can-we-improvelearn-how-to-draw-a-realistic-face-shaun-and-mikey-download.jpg" alt="" />
                        </div>
                        <div className="user-container--name">{texts.designer_name}, expert-designer</div>
                    </div>
                </div>
                <h1 className="product-info--header minus-plus-icon">Product details<em></em></h1>
                <div className="product-info--text">
                    <p>Day-to-night timeless style that celebrates your curves. Matte reclaimed wool blend that retains color and moves with you. 3/4 sleeves and round neck keep it chic.</p>
                    <div className="characteristics-list">
                        <p>25" from top to hem</p>
                        <p>2" slit in the back</p>
                        <p>Keyhole design in the back</p>
                        <p>Hits just below your knee for maximum versatility</p>
                    </div>
                    <div className="product-data">
                        <div className="product-data--line">
                            <div className="product-data--title">Material</div>
                            <div className="product-data--value characteristics-list">
                                <p>30-45% Wool</p>
                                <p>50-69% Cotton</p>
                                <p>1-5% Silk</p>
                            </div>
                        </div>
                        <div className="product-data--line">
                            <div className="product-data--title">Feel</div>
                            <div className="product-data--value">Soft, substantial, matte</div>
                        </div>
                        <div className="product-data--line">
                            <div className="product-data--title">Care</div>
                            <div className="product-data--value">Dry clean only</div>
                        </div>
                        <div className="product-data--line">
                            <div className="product-data--title">Fit</div>
                            <div className="product-data--value">Skims your curves without restricting them</div>
                        </div>
                    </div>
                </div>
                <h1 className="product-info--header minus-plus-icon">Delivery & returns<em></em></h1>
                <div className="product-info--text">
                    <div className="characteristics-list">
                        <p>Your dress will be delivered in 12 days after you fully pay for the item.</p>
                        <p>Easy returns during first two weeks.</p>
                    </div>
                </div>
            </section>
        </main>
    );
};
/*class Product extends Component {
    render() {
        return (

        );
    }
}*/
export default Product;