import React, {Component} from 'react';
import ReactDOM from 'react-dom';
// import '../styles/feedback.scss';
//import screenshot from '../images/tmp/screenshot-2018-06-16-14-45-59.jpg';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom';
const FeedbackSent = () => {
    return (
        <main>
            <section className="container-left">
                <Link to="/feedback" className="icon-back"></Link>
            </section>
            <section className="container-right feedback feedback-sent">
                <div className="feedback-sent--header">
                    <h1 className="">Got it.</h1>
                    <p>Please expect your updated design in 24 hours.</p>
                </div>
                <div className="product-image">
                    <img src='http://meganbrookehandmadeblog.com/wp-content/uploads/2012/08/Fall-Fashion-2.jpg' />
                    {/*<img src={screenshot} />*/}
                    <div className="zoom-icon"></div>
                </div>
                <div className="feedback--container">
                    <div className="feedback-sent--message">
                        <p>For most people, the ringing of a phone was a welcome sign. Someone was trying to reach them, to say hello, ask about their well-being, or make plans. For me, it triggered fear, intense anxiety and heart-stopping panic.</p>
                        <div className="feedback--images">
                            <div className="feedback--img">
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSseMRa3-uayB0xWs7FYbmWTpW-pNXC3ty54-WPbkbeQf-AmnK4Sg" />
                            </div>
                            <div className="feedback--img">
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSseMRa3-uayB0xWs7FYbmWTpW-pNXC3ty54-WPbkbeQf-AmnK4Sg" />
                            </div>
                            <div className="feedback--img">
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCxkmLLIBz3qME3C3ibZOYIpvdskOvJt7L8o6279RW3iZ8c2nN" />
                            </div>
                        </div>
                    </div>
                    <div className="user-avatar">
                        <img alt="userName" src="http://dont-touch.me/wp-content/uploads/2018/04/learn-to-draw-realistic-faces-can-we-improvelearn-how-to-draw-a-realistic-face-shaun-and-mikey-download.jpg" />
                    </div>
                    <div className="buttons">
                        <Link to="/prepay" className="button">Done</Link>
                        <Link to="/feedback" className="button btn-white">Add more</Link>
                    </div>
                </div>
            </section>
        </main>
    );
}

/*class FeedbackSent extends Component {

    render() {
        return (

        )
    }
}*/

export default FeedbackSent;