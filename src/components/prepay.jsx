import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom';
import PaymentInfo from './payment-info.jsx';

class Prepay extends Component {

    render() {
        return (
            <main>
                <h1 className="">Pre-Payment</h1>
                <section className="container-left prepay">
                    <form action="" className="prepay-form">
                        <input className="text-input" type="text" placeholder="Card number" />
                        <input className="text-input" type="text" placeholder="Cardholder name" />
                        <div className="text-inputs">
                            <input className="text-input" type="text" placeholder="MM / YY" />
                            <input className="text-input" type="text" placeholder="CVV" />
                        </div>
                        <div className="buttons">
                            <Link to="/delivery" className="button one-button">Pay</Link>
                        </div>
                    </form>
                    <div className="prepay-message">
                        <p>Full refund is guarantee if you don’t like my design.</p>
                        <div className="user-container">
                            <div className="user-avatar">
                                <img src="http://dont-touch.me/wp-content/uploads/2018/04/learn-to-draw-realistic-faces-can-we-improvelearn-how-to-draw-a-realistic-face-shaun-and-mikey-download.jpg" alt="" />
                            </div>
                            <div className="user-container--name">Erica, expert-designer</div>
                        </div>
                    </div>
                </section>
                <section className="container-right prepay">
                    <PaymentInfo/>
                </section>
            </main>
        )
    }
}

export default Prepay;