import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom';
 // import '../styles/sizes.scss';

class Sizes extends Component {

    render() {
        return (
            <main>
                <section className="container-centered sizes">
                    <Link to="/" className="icon-back"></Link>
                    <h1 className="sizes--title">To better sew a Dress for you, let me know your sizes</h1>
                    <div className="sizes--numbers">
                        <p>Please, choose your size (US)</p>
                        <div className="sizes--container">
                            <div className="sizes--item">2</div>
                            <div className="sizes--item">4</div>
                            <div className="sizes--item">6</div>
                            <div className="sizes--item active">8</div>
                            <div className="sizes--item">10</div>
                            <div className="sizes--item">12</div>
                            <div className="sizes--item">14</div>
                            <div className="sizes--item">16</div>
                            <div className="sizes--item">18</div>
                            <div className="sizes--item">20</div>
                        </div>
                        <Link to="/size-guide" className="sizes--size-guide">Size guide</Link>
                    </div>
                    <div className="button-checkbox active">I need a measurement session</div>
                    <div className="sizes--grey-text">Our professional tailor will measure you over the video call. I will offer you the time-slots after you’ll complete your checkout.</div>
                    <div className="buttons">
                        <Link className="button one-button" to="/prepay">Confirm</Link>
                    </div>
                </section>
            </main>
        )
    }
}

export default Sizes;