import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import {
    BrowserRouter,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom';
import Product from './product.jsx';
import Feedback from './feedback.jsx';
import FeedbackSent from './feedback-sent.jsx';
import Sizes from './sizes.jsx';
import SizeGuide from './size-guide.jsx';
import Header from './header.jsx';
import Prepay from './prepay.jsx';
import Delivery from './delivery.jsx';
import '../styles/app.scss';


const links = [
    {
        link: '/feedback',
        label: 'feedback'
    }
];

export var texts =
    {
        price: '$175',
        title: 'Cotton Stretch Dress',
        image_url: 'http://meganbrookehandmadeblog.com/wp-content/uploads/2012/08/Fall-Fashion-2.jpg',
        designer_name: 'Erica',
        designer_message: 'designer_message',
        product_details: 'details',
        material: 'material',
        feel: 'feel',
        care: 'care',
        fit: 'fit',
        designer_image_url: 'designer_image_url'
    };


    /*image_list = {
        1: 'http://dont-touch.me/wp-content/uploads/2018/04/learn-to-draw-realistic-faces-can-we-improvelearn-how-to-draw-a-realistic-face-shaun-and-mikey-download.jpg',
        2: 'http://meganbrookehandmadeblog.com/wp-content/uploads/2012/08/Fall-Fashion-2.jpg'
    };*/


class App extends Component {
  render() {
    return (
            <div className="App">
                <Header />
                {/*<div className="menu-tmp">
                    <Link to="/">Product</Link>
                    <Link to="/feedback">Feedback</Link>
                    <Link to="/feedback-sent">Feedback sent</Link>
                    <Link to="/sizes">Sizes</Link>
                    <Link to="/size-guide">Size Guide</Link>
                </div>*/}

                {/*<Product items={links} />*/}
                    <Switch>
                        {/*<Route exact path="/views/fullpay" component={Product} />*/}
                        <Route exact path="/" component={Product} />
                        <Route path="/feedback" component={Feedback} />
                        <Route path="/feedback-sent" component={FeedbackSent} />
                        <Route path="/sizes" component={Sizes} />
                        <Route path="/size-guide" component={SizeGuide} />
                        <Route path="/prepay" component={Prepay} />
                        <Route path="/delivery" component={Delivery} />
                    </Switch>
            </div>
	);
  }
}

export default App;

if (module.hot) {
  module.hot.accept();
}


/*
price
title
image_url
designer_name
designer_message
product_details
material
feel
care
fit
designer_image_url
image_list (масив линков на доп. изображения)
*/
