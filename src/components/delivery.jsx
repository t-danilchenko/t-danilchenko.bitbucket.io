import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom';
import PaymentInfo from './payment-info.jsx';

class Delivery extends Component {

    render() {
        return (
            <main>
                <div className="delivery-title">
                    <h1 className="">Jenny, your Skirt is almost ready to be made!</h1>
                    <p>Let&apos;s clarify the details of delivery &amp; pay.</p>
                </div>
                <section className="container-left delivery">
                    <form action="" className="delivery-form active">
                        <div className="delivery-form--title">
                            <div className="delivery-form--number">1</div>
                            Shipping Address
                        </div>
                            <input className="text-input" type="text" placeholder="First Name" />
                            <input className="text-input" type="text" placeholder="Second name" />
                            <input className="text-input" type="text" placeholder="Email" />
                            <div className="text-inputs">
                                <input className="text-input" type="text" placeholder="Area Code" />
                                <input className="text-input" type="text" placeholder="Primary Phone" />
                            </div>
                            <input className="text-input" type="text" placeholder="Street Address" />
                            <div className="text-inputs">
                                <input className="text-input" type="text" placeholder="ZIP Code" />
                                <div className="text-inputs--text">
                                    Enter ZIP for City and State
                                </div>
                            </div>
                        <div className="delivery-form--container">
                            <div className="delivery-form--subtitle">Our Shipping Policy</div>
                            <p>Each Epytom piece is designed & custom-made just for you. Which means up to 8 days to produce. Expect it to land at your door in 12 days or less — and look out for tracking when it's on its way! Horray to slow fashion that's all about you!</p>
                            <ul className="delivery-form--ul">
                                <li>Custom-designed and made just for you</li>
                                <li>Delivery in 12 days</li>
                                <li>Free, easy returns</li>
                            </ul>
                        </div>
                            <div className="buttons">
                                <Link to="/delivery" className="button one-button">Continue</Link>
                            </div>
                    </form>
                    <form action="" className="delivery-form delivery-form--continue active">
                        <div className="delivery-form--title">
                            <div className="delivery-form--number">2</div>
                            Shipping Method
                        </div>
                        <div className="delivery-form--container">
                            <label className="delivery-form--label">
                                <input type="radio" />
                                Ground Shipping (UPS) Free
                            </label>
                            <label className="delivery-form--label">
                                <input type="radio" />
                                Overnight $25
                            </label>
                        </div>
                        <div className="buttons">
                            <Link to="/delivery" className="button one-button">Continue</Link>
                        </div>
                    </form>
                    <form action="" className="delivery-form delivery-form--payment active">
                        <div className="delivery-form--title">
                            <div className="delivery-form--number">3</div>
                            Payment
                        </div>
                        <div className="delivery-form--container">
                                <div className="quote"></div>
                                <p>I’m getting ready to sew your one-of-kind dress! You will get it delivered in 8-12 days. Should I process the full-payment of $97.43? Btw you can always get a full refund with us!</p>
                            <div className="user-container">
                                <div className="user-avatar">
                                    <img src="http://dont-touch.me/wp-content/uploads/2018/04/learn-to-draw-realistic-faces-can-we-improvelearn-how-to-draw-a-realistic-face-shaun-and-mikey-download.jpg" alt="" />
                                </div>
                                <div className="user-container--name">Erica, expert-designer</div>
                            </div>
                        </div>
                        <div className="buttons">
                            <Link to="/" className="button one-button">Yes, I authorise the charge</Link>
                        </div>
                    </form>
                </section>
                <section className="container-right prepay">
                    <PaymentInfo/>
                </section>
            </main>
        )
    }
}

export default Delivery;