import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom';

class PaymentInfo extends Component {

    render() {
        return (
            <div className="prepay-info">
                <div className="prepay-info--line">
                    <div className="prepay-info--title">Title</div>
                    <div className="prepay-info--value">$456.87</div>
                </div>
                <div className="prepay-info--line">
                    <div className="prepay-info--title">Full price</div>
                    <div className="prepay-info--value">$150.87</div>
                </div>
                <div className="prepay-info--line">
                    <div className="prepay-info--title">Item discount</div>
                    <div className="prepay-info--value">-$6.87</div>
                </div>
            </div>
        )
    }
}

export default PaymentInfo;