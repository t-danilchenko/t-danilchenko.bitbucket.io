import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom';
//import screenshot from '../images/tmp/screenshot-2018-06-16-14-45-59.jpg';
// import '../styles/feedback.scss';

const Feedback = () => {
    return (
        <main>
            <section className="container-left">
                <Link to="/" className="icon-back"></Link>
            </section>
            <section className="container-right feedback">
                <h1 className="">Leave feedback</h1>
                <div className="product-image">
                    <img src='http://meganbrookehandmadeblog.com/wp-content/uploads/2012/08/Fall-Fashion-2.jpg' />
                    {/*<img src={screenshot} />*/}
                    <div className="zoom-icon"></div>
                </div>
                <div className="feedback--container">
                    <textarea className="textarea" placeholder="Write your message">
                    </textarea>
                    <div className="feedback--images">
                        <div className="feedback--img">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSseMRa3-uayB0xWs7FYbmWTpW-pNXC3ty54-WPbkbeQf-AmnK4Sg" />
                        </div>
                        <div className="feedback--img">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSseMRa3-uayB0xWs7FYbmWTpW-pNXC3ty54-WPbkbeQf-AmnK4Sg" />
                        </div>
                        <div className="feedback--img">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCxkmLLIBz3qME3C3ibZOYIpvdskOvJt7L8o6279RW3iZ8c2nN" />
                        </div>
                    </div>
                    <div className="buttons">
                        <Link to="/feedback-sent" className="button">Update my design</Link>
                        <button className="button btn-white btn-camera"></button>
                    </div>
                </div>
            </section>
        </main>
    );
}

export default Feedback;