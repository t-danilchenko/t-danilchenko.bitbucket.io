import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom';
// import '../styles/header.scss';

class Header extends Component {

    render() {
        return (
            <header>
                <Link to="/" className="logo" title="Epytom">Epytom</Link>
                <div className="header-user">
                    <div className="header-user--name">userName</div>
                    <div className="header-user--avatar user-avatar">
                        <img alt="userName" src="http://dont-touch.me/wp-content/uploads/2018/04/learn-to-draw-realistic-faces-can-we-improvelearn-how-to-draw-a-realistic-face-shaun-and-mikey-download.jpg" />
                    </div>
                </div>
            </header>
        )
    }
}

export default Header;