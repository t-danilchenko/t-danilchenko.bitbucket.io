import React, {Component} from 'react';
import ReactDOM from 'react-dom';

// import '../styles/sizes.scss';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom';
class SizeGuide extends Component {

    render() {
        return (
            <main>
                <section className="modal size-guide">
                    {/*<div className="close-icon"></div>*/}
                    <Link to="/sizes" className="close-icon"></Link>
                    <h1 className="">Size guide</h1>
                    <table className="size-guide--table">
                        <thead>
                            <tr>
                                <th>US size</th>
                                <th>Bust</th>
                                <th>Waist</th>
                                <th>Hips</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>0</td>
                                <td>32"</td>
                                <td>24.5"</td>
                                <td>34"</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>32"</td>
                                <td>24.5"</td>
                                <td>34"</td>
                            </tr>
                        </tbody>
                    </table>
                </section>
            </main>
        )
    }
}

export default SizeGuide;