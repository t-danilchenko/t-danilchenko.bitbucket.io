/*var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');
//const HtmlWebpackPlugin = require('html-webpack-plugin');
//const fs = require('fs')

//function generateHtmlPlugins(templateDir) {
//  const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir));
//  return templateFiles.map(item => {
//    const parts = item.split('.');
//    const name = parts[0];
 //   const extension = parts[1];
//    return new HtmlWebpackPlugin({
//      filename: `${name}.html`,
//      template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`),
//      inject: false,
//    })
//  })
//}

//const htmlPlugins = generateHtmlPlugins('./src/html/views');

//const extractPlugin = new ExtractTextPlugin({
//   filename: 'bundle.css'
//});


var HtmlWebPackPlugin = require("html-webpack-plugin");
var htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html",
  filename: "./index.html"
});

module.exports = {
  entry: [
      './index.js',
      './src/scss/style.scss'
  ],
  output: {
	  //path: path.resolve(‘dist’),
	  path: path.resolve(__dirname, 'dist')
  },
  //devtool: "source-map",
  resolve: {
		extensions: ['', '.js', '.jsx'],
	},
  module: {
    rules: [{
        test: /\.js$/,
		//exclude: /node_modules/,
        //include: path.resolve(__dirname, 'src/js'),

        use: {
            loader: 'babel-loader',
            options: {
                presets: 'env'
				//presets: ['env', 'es2015', 'react']
            }
          }
        },
        {
          test: /\.(sass|scss)$/,
          include: path.resolve(__dirname, 'src/scss'),
          use: ExtractTextPlugin.extract({
            use: [{
                    loader: "css-loader",
                    options: {
						modules: true,
						importLoaders: 1,
						//localIdentName: "[name]_[local]_[hash:base64]",
                        sourceMap: true,
                        minimize: true,
                        url: false
                    }
                },
                {
                    loader: "sass-loader",
                    options: {
                        sourceMap: true
                    }
                }
            ]
        })
      },
//	  {
//        test: /\.html$/,
//        include: path.resolve(__dirname, 'src/html/includes'),
//        use: ['raw-loader']
//      },
    ],
	loaders: [{
				test: /\.jsx$/, 
				loader: 'jsx-loader'
				},
			]
  },
  plugins: [
      new ExtractTextPlugin({
          filename: './css/style.bundle.css',
          allChunks: true,
      }),
	  new CopyWebpackPlugin([{
        from: './src/fonts',
        to: './fonts'
      },
      {
        from: './src/favicon',
        to: './favicon'
      },
      {
        from: './src/img',
        to: './img'
      }
    ]),
		new HtmlWebPackPlugin({
			template: "./src/index.html",
			filename: "./index.html"
		  });
  ]
};*/
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
      app: path.join(__dirname, 'src', 'index.js')
  },
  output: {
    path: path.join(__dirname, 'build')/*,
      filename: 'bundle.js'*/
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(jpe?g|png|gif)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000
          }
        }]
      },
      {
        test: /\.(eot|svg|ttf|woff2?|otf)$/,
        use: 'file-loader'
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'src', 'index.html'),
      favicon: path.join(__dirname, 'src', 'favicon.ico')
    })
  ]
};